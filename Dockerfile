FROM golang:1.22.2-alpine3.19 as builder
ARG VERSION=${CI_COMMIT_TAG}

WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download -x

COPY . .

RUN GOOS=linux GOARCH=amd64 \
    go build -ldflags "-X 'main.Version=${VERSION}' -s -w" -o exchanger \
    ./cmd/exchanger/*

FROM --platform=linux/amd64 alpine:3.19
WORKDIR /app
COPY --from=builder /build/exchanger ./
CMD ["./exchanger"]
