package main

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"github.com/spf13/pflag"

	"exchanger/internal/config"
	"exchanger/internal/repository"
	"exchanger/internal/service"
	"exchanger/internal/transport/http"
	"exchanger/internal/transport/http/client"
	"exchanger/internal/transport/http/handlers"
	"exchanger/internal/transport/http/metric"
	middleware "exchanger/internal/transport/http/middlewares"
	"exchanger/internal/transport/http/router"
	"exchanger/internal/worker"
	"exchanger/pkg/logger"
)

type (
	BackGroundWorker interface {
		Run(ctx context.Context)
		Shutdown()
	}
)

var (
	Version    = "v0.0.0"
	configPath = pflag.String("config", "/etc/exchanger/config.toml", "path to config")
)

func main() {
	pflag.Parse()

	ctx, cancelCtx := context.WithCancel(context.Background())

	cfg, err := config.ParseConfig(*configPath)
	if err != nil {
		slog.Error("Can't parse config", slog.String("error", err.Error()))
		os.Exit(-1)
	}

	metric.Info.WithLabelValues(Version).Set(1)
	slog.SetDefault(
		slog.New(logger.GetLogHandler(cfg.Main.LogFormat, cfg.Main.LogLevel)),
	)

	connectionGORM, err := setupDatabaseConnection(cfg.Database)
	if err != nil {
		slog.Error("Can't setup database connection", slog.String("error", err.Error()))
		os.Exit(-1)
	}

	slog.Info(
		"Successfully connected to database",
		slog.String("host", cfg.Database.Host),
		slog.String("database", cfg.Database.Database),
	)

	rateRepository := repository.NewRatesRepository(connectionGORM)
	currencyRepository := repository.NewCurrenciesRepository(connectionGORM)
	fastForex := client.NewFastForex(cfg.FastForex.ApiKey, cfg.FastForex.Address)

	bg := []BackGroundWorker{
		worker.NewCurrencyRateUpdater(
			service.NewRateService(rateRepository),
			service.NewFastForexService(fastForex, currencyRepository),
			cfg.FastForex.UpdatePeriod,
		),
	}

	for _, backGroundWorker := range bg {
		backGroundWorker.Run(ctx)
		defer backGroundWorker.Shutdown()
	}

	convertHandlers := handlers.NewExchangeHandler(
		service.NewRateService(rateRepository),
	)

	convertRouter := router.NewRouter(
		router.Handlers{
			Convert: convertHandlers.GetConvert,
		},
		middleware.NewFactory(),
	)

	server := http.NewServer(cfg.Main.PublicHTTPAddress, convertRouter)

	errChannel := make(chan error)

	go func() {
		slog.Info("Starting public http server", slog.String("address", cfg.Main.PublicHTTPAddress))
		errChannel <- server.Run()
	}()

	go func() {
		slog.Info("Starting private http server", slog.String("address", cfg.Main.PrivateHTTPAddress))
		errChannel <- startPrivateServer(cfg.Main.PrivateHTTPAddress)
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	select {
	case sig := <-signalChan:
		slog.Info("Shutting down due to signal", slog.String("signal", sig.String()))
		cancelCtx()
		os.Exit(0)

	case err := <-errChannel:
		slog.Error("Error while serving connection", slog.String("error", err.Error()))
		cancelCtx()
		os.Exit(-1)
	}
}
