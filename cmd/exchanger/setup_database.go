package main

import (
	"database/sql"
	"fmt"
	"log/slog"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"exchanger/internal/config"
	"exchanger/pkg/postgresql/migrator"
)

const (
	postgresDriver = "postgres"
)

func setupDatabaseConnection(databaseCfg config.DataBase) (*gorm.DB, error) {
	connectionString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		databaseCfg.Host, databaseCfg.Port, databaseCfg.User, databaseCfg.Password, databaseCfg.Database)

	connectionDB, err := sql.Open(postgresDriver, connectionString)
	if err != nil {
		return nil, fmt.Errorf("can't open connection: %w", err)
	}

	if err = connectionDB.Ping(); err != nil {
		return nil, fmt.Errorf("can't ping database: %w", err)
	}

	migrator := migrator.NewMigrator(connectionDB, slog.Default())

	if err := migrator.Up(); err != nil {
		return nil, fmt.Errorf("can't migrate schema: %w", err)
	}

	connectionGORM, err := gorm.Open(postgres.New(postgres.Config{
		Conn: connectionDB,
	}))
	if err != nil {
		return nil, err
	}

	return connectionGORM, nil
}
