package main

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/adaptor"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func startPrivateServer(address string) error {
	app := fiber.New()

	app.Get("/livez", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(http.StatusOK)
	})

	app.Get("/readyz", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(http.StatusOK)
	})

	app.Get("/metrics", adaptor.HTTPHandler(promhttp.Handler()))

	return app.Listen(address)
}
