package config

import (
	"time"

	"github.com/BurntSushi/toml"
)

type (
	Config struct {
		Main      Main      `toml:"main"`
		Database  DataBase  `toml:"database"`
		FastForex FastForex `toml:"fast_forex"`
	}

	Main struct {
		LogLevel           string `toml:"log_level"`
		LogFormat          string `toml:"log_format"`
		PublicHTTPAddress  string `toml:"public_http_address"`
		PrivateHTTPAddress string `toml:"private_http_address"`
	}

	DataBase struct {
		Host     string `toml:"host"`
		Port     uint16 `toml:"port"`
		User     string `toml:"user"`
		Password string `toml:"password"`
		Database string `toml:"database"`
	}

	FastForex struct {
		ApiKey       string        `toml:"api_key"`
		Address      string        `toml:"address"`
		UpdatePeriod time.Duration `toml:"update_period"`
	}
)

func ParseConfig(path string) (*Config, error) {
	var cfg Config
	if _, err := toml.DecodeFile(path, &cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
