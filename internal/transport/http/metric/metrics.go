package metric

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const (
	prometheusNamespacePrefix = "exchanger"
)

var (
	Info = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: prometheusNamespacePrefix,
		Name:      "info",
	}, []string{"version"})

	CountHandledHTTPRequests = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: prometheusNamespacePrefix,
			Name:      "handled_requests_total",
			Help:      "Total count of error responses",
		},
		[]string{"http_endpoint", "status"},
	)

	ResponseTimeHistogramOperationSeconds = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: prometheusNamespacePrefix,
			Name:      "requests_processing_duration_seconds",
			Help:      "Duration of processing HTTP requests in seconds",
		},
		[]string{"http_endpoint"},
	)
)
