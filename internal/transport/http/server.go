package http

import (
	"context"
	"time"

	"github.com/gofiber/fiber/v2"
)

type (
	Router interface {
		Init(app *fiber.App)
	}

	server struct {
		fiber   *fiber.App
		address string
		routers []Router
	}
)

func NewServer(address string, routers ...Router) *server {
	s := server{
		fiber: fiber.New(fiber.Config{
			CaseSensitive: true,
			AppName:       "exchanger",
			ReadTimeout:   30 * time.Second,
			WriteTimeout:  30 * time.Second,
		}),
		routers: routers,
		address: address,
	}

	for i := range s.routers {
		s.routers[i].Init(s.fiber)
	}

	return &s
}

func (s *server) Run() error {
	return s.fiber.Listen(s.address)
}

func (s *server) Shutdown(ctx context.Context) error {
	return s.fiber.ShutdownWithContext(ctx)
}
