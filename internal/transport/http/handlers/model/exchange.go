package model

import "github.com/shopspring/decimal"

type (
	// ConvertRequestParams represents the request parameters for converting.
	// swagger:parameters GetConvert
	ConvertRequestParams struct {
		Amount       float64 `query:"amount" validate:"required,gt=0"`
		ToCurrency   string  `query:"to_currency" validate:"required"`
		FromCurrency string  `query:"from_currency" validate:"required"`
	}
)

type (
	// ErrorResponse  provides a standardized error response.
	// swagger:response ErrorResponse
	ErrorResponse struct {
		Error string `json:"error"`
	}

	// Response represents the response for the conversion.
	// swagger:response Response
	Response struct {
		Amount decimal.Decimal `json:"amount"`
	}
)
