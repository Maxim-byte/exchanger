package handlers

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/shopspring/decimal"

	serviceModel "exchanger/internal/service/model"
	"exchanger/internal/transport/http/handlers/model"
)

type (
	RateService interface {
		GetRate(ctx context.Context, fromCurrency, toCurrency string) (float64, error)
	}

	exchangeHandler struct {
		validator   *validator.Validate
		rateService RateService
	}
)

func NewExchangeHandler(service RateService) *exchangeHandler {
	return &exchangeHandler{validator: validator.New(), rateService: service}
}

// GetConvert converts the amount from one currency to another.
// swagger:route GET /api/v1/convert GetConvert
// Consumes:
// - application/json
// Responses:
//   200: Response
//   400: ErrorResponse
//   500: ErrorResponse

func (h *exchangeHandler) GetConvert(c *fiber.Ctx) error {
	var queryParams model.ConvertRequestParams
	if err := c.QueryParser(&queryParams); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(model.ErrorResponse{Error: fmt.Sprintf("invalid query params: %v", err)})
	}

	if err := h.validator.Struct(queryParams); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(model.ErrorResponse{Error: fmt.Sprintf("invalid query params: %v", err)})
	}

	rate, err := h.rateService.GetRate(c.UserContext(), queryParams.FromCurrency, queryParams.ToCurrency)
	switch {
	case errors.Is(err, serviceModel.ErrNotFound):
		return c.Status(http.StatusNotFound).JSON(model.ErrorResponse{Error: fmt.Sprintf("rate not found for %s to %s", queryParams.FromCurrency, queryParams.ToCurrency)})
	case err != nil:
		return c.Status(http.StatusInternalServerError).JSON(model.ErrorResponse{Error: err.Error()})
	}

	return c.JSON(model.Response{Amount: decimal.NewFromFloat(rate).Mul(decimal.NewFromFloat(queryParams.Amount))})
}
