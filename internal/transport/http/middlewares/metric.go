package middleware

import (
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"

	"exchanger/internal/transport/http/metric"
)

type (
	MetricsMiddleware struct {
	}
)

func NewMetricsMiddleware() *MetricsMiddleware {
	return &MetricsMiddleware{}
}

func (m *MetricsMiddleware) Handle(c *fiber.Ctx) error {
	path := c.Path()

	startOperationTime := time.Now()
	defer func() {
		metric.ResponseTimeHistogramOperationSeconds.WithLabelValues(path).Observe(time.Since(startOperationTime).Seconds())

		metric.CountHandledHTTPRequests.WithLabelValues(
			path,
			strconv.FormatInt(int64(c.Response().StatusCode()), 10),
		).Inc()
	}()

	return c.Next()
}
