package middleware

type (
	Factory struct{}
)

func NewFactory() *Factory {
	return &Factory{}
}

func (m *Factory) NewMetricsMiddleware() *MetricsMiddleware { return NewMetricsMiddleware() }
