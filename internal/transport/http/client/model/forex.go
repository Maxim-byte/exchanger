package model

type (
	Rates struct {
		Rates map[string]float64
	}

	GetFiatRatesResponse struct {
		Rates map[string]float64 `json:"results"`
	}

	GetCryptoRatesResponse struct {
		Rates map[string]float64 `json:"prices"`
	}

	GetSupportedCurrenciesResponse struct {
		Currencies map[string]string `json:"currencies"`
	}
)
