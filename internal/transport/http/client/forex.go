package client

import (
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/shopspring/decimal"

	"exchanger/internal/transport/http/client/model"
)

type (
	fastForex struct {
		apiKey                    string
		apiAddress                string
		client                    *http.Client
		supportedFiatCurrencies   map[string]struct{}
		supportedCryptoCurrencies map[string]struct{}
	}
)

func NewFastForex(apiKey, apiAddress string) *fastForex {
	fastForex := &fastForex{
		apiKey:                    apiKey,
		apiAddress:                apiAddress,
		client:                    &http.Client{Timeout: time.Second * 10},
		supportedFiatCurrencies:   make(map[string]struct{}),
		supportedCryptoCurrencies: make(map[string]struct{}),
	}

	expBackoff := backoff.NewExponentialBackOff()
	expBackoff.MaxElapsedTime = 0

	_ = backoff.Retry(func() error {
		return fastForex.getSupportedFiatCurrencies()
	}, expBackoff)

	return fastForex
}

func (s *fastForex) GetCryptoRates(fromCurrency string, toCurrencies []string) (model.Rates, error) {
	supportedCurrencies := append([]string{}, toCurrencies...)

	if !s.checkIfSupportedCurrencies(append(supportedCurrencies, fromCurrency)) {
		return model.Rates{}, fmt.Errorf("unsupported currency")
	}

	pairs := make([]string, 0, len(toCurrencies))
	for _, toCurrency := range toCurrencies {
		pairs = append(pairs, fmt.Sprintf("%s/%s", fromCurrency, toCurrency))
	}

	url := fmt.Sprintf("%s/crypto/fetch-prices?pairs=%s&api_key=%s",
		s.apiAddress, strings.Join(pairs, ","), s.apiKey)

	response, err := s.client.Get(url)
	if err != nil {
		return model.Rates{}, fmt.Errorf("failed to get rate: %w", err)
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return model.Rates{}, fmt.Errorf("failed to get rate: code : %d", response.StatusCode)
	}

	var result model.GetCryptoRatesResponse
	if err := json.NewDecoder(response.Body).Decode(&result); err != nil {
		return model.Rates{}, fmt.Errorf("failed to decode response: %w", err)
	}

	ratesResponse := model.Rates{Rates: make(map[string]float64)}
	for key, value := range result.Rates {
		ratesResponse.Rates[strings.Split(key, "/")[1]] = value
	}

	return ratesResponse, nil
}

func (s *fastForex) GetFiatRates(fromCurrency string, toCurrencies []string) (model.Rates, error) {
	supportedCurrencies := append([]string{}, toCurrencies...)

	if !s.checkIfSupportedCurrencies(append(supportedCurrencies, fromCurrency)) {
		return model.Rates{}, fmt.Errorf("unsupported currency")
	}

	pairs := make([]string, 0, len(toCurrencies))
	for _, toCurrency := range toCurrencies {
		pairs = append(pairs, fmt.Sprintf("%s/%s", toCurrency, fromCurrency))
	}

	url := fmt.Sprintf("%s/crypto/fetch-prices?pairs=%s&api_key=%s",
		s.apiAddress, strings.Join(pairs, ","), s.apiKey)

	response, err := s.client.Get(url)
	if err != nil {
		return model.Rates{}, fmt.Errorf("failed to get rate: %w", err)
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return model.Rates{}, fmt.Errorf("failed to get rate: code : %d", response.StatusCode)
	}

	var result model.GetCryptoRatesResponse
	if err := json.NewDecoder(response.Body).Decode(&result); err != nil {
		return model.Rates{}, fmt.Errorf("failed to decode response: %w", err)
	}

	ratesResponse := model.Rates{Rates: make(map[string]float64, len(result.Rates))}
	for key, rate := range result.Rates {
		invertedRate := decimal.NewFromInt(1).Div(decimal.NewFromFloat(rate))
		invertedRateFloat, _ := invertedRate.Float64()
		ratesResponse.Rates[strings.Split(key, "/")[0]] = invertedRateFloat
	}

	return ratesResponse, nil
}

func (s *fastForex) getSupportedFiatCurrencies() error {
	fiatResponse, err := s.client.Get(fmt.Sprintf("%s/currencies?api_key=%s", s.apiAddress, s.apiKey))
	if err != nil {
		slog.Error("failed to get supported fiat currencies", slog.String("error", err.Error()))
		return fmt.Errorf("failed to get supported fiat currencies: %w", err)
	}
	defer fiatResponse.Body.Close()

	if fiatResponse.StatusCode != http.StatusOK {
		slog.Error("failed to get supported fiat currencies", slog.Int("code", fiatResponse.StatusCode))
		return fmt.Errorf("failed to get supported fiat currencies: code : %d", fiatResponse.StatusCode)
	}

	var result model.GetSupportedCurrenciesResponse
	if err := json.NewDecoder(fiatResponse.Body).Decode(&result); err != nil {
		return fmt.Errorf("failed to decode response: %w", err)
	}

	for currency := range result.Currencies {
		s.supportedFiatCurrencies[currency] = struct{}{}
	}

	cryptoResponse, err := s.client.Get(fmt.Sprintf("%s/crypto/currencies?api_key=%s", s.apiAddress, s.apiKey))
	if err != nil {
		slog.Error("failed to get supported fiat currencies", slog.String("error", err.Error()))
		return fmt.Errorf("failed to get supported fiat currencies: %w", err)
	}
	defer cryptoResponse.Body.Close()

	if cryptoResponse.StatusCode != http.StatusOK {
		slog.Error("failed to get supported fiat currencies", slog.Int("code", fiatResponse.StatusCode))
		return fmt.Errorf("failed to get supported crypto currencies: code : %d", cryptoResponse.StatusCode)
	}

	if err := json.NewDecoder(cryptoResponse.Body).Decode(&result); err != nil {
		return fmt.Errorf("failed to decode response: %w", err)
	}

	for currency := range result.Currencies {
		s.supportedCryptoCurrencies[currency] = struct{}{}
	}

	return nil
}

func (s *fastForex) checkIfSupportedCurrencies(currencies []string) bool {
	for _, currency := range currencies {
		_, isFiat := s.supportedFiatCurrencies[currency]
		_, isCrypto := s.supportedCryptoCurrencies[currency]

		if !isFiat && !isCrypto {
			return false
		}
	}

	return true
}
