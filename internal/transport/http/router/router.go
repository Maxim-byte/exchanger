package router

import (
	"github.com/gofiber/fiber/v2"

	middleware "exchanger/internal/transport/http/middlewares"
)

type (
	Handlers struct {
		Convert fiber.Handler
	}

	router struct {
		handlers           Handlers
		middlewaresFactory *middleware.Factory
	}
)

func NewRouter(handlers Handlers, factory *middleware.Factory) *router {
	return &router{
		handlers:           handlers,
		middlewaresFactory: factory,
	}
}

func (r *router) Init(app *fiber.App) {
	convertGroup := app.Group("api/v1").
		Use(r.middlewaresFactory.NewMetricsMiddleware().Handle)

	convertGroup.Get("convert", r.handlers.Convert)
}
