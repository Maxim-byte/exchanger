package service

import (
	"context"
	"fmt"
	"sync"

	"exchanger/internal/domain"
)

type (
	rateService struct {
		mu        sync.RWMutex
		cacheRate map[string]float64
		ratesRepo RateRepository
	}
)

func NewRateService(ratesRepo RateRepository) *rateService {
	return &rateService{
		ratesRepo: ratesRepo,
	}
}

func (s *rateService) GetRate(ctx context.Context, fromCurrency, toCurrency string) (float64, error) {
	s.mu.RLock()

	if rate, ok := s.cacheRate[fmt.Sprintf("%s-%s", fromCurrency, toCurrency)]; ok {
		s.mu.RUnlock()
		return rate, nil
	}
	s.mu.RUnlock()

	rate, err := s.ratesRepo.GetRate(ctx, fromCurrency, toCurrency)
	if err != nil {
		return 0, fmt.Errorf("failed to get rate: %w", err)
	}

	return rate.Rate, nil
}

func (s *rateService) UpdateRates(ctx context.Context, rates []domain.Rate) error {
	go s.updateCacheRates(rates)
	return s.ratesRepo.UpdateRates(ctx, rates)
}

func (s *rateService) updateCacheRates(rates []domain.Rate) {
	tempCache := make(map[string]float64, len(rates))

	for _, rate := range rates {
		tempCache[fmt.Sprintf("%s-%s", rate.FromCurrency, rate.ToCurrency)] = rate.Rate
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	s.cacheRate = tempCache
}
