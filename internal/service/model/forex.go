package model

const (
	FiatCurrencyType   = "fiat"
	CryptoCurrencyType = "crypto"
)

type (
	Rate struct {
		FromCurrency string
		ToCurrencies map[string]float64
	}
)
