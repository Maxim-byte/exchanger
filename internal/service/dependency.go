package service

import (
	"context"

	"exchanger/internal/domain"
	"exchanger/internal/transport/http/client/model"
)

type (
	RateRepository interface {
		UpdateRates(ctx context.Context, rates []domain.Rate) error
		GetRate(ctx context.Context, fromCurrency string, toCurrency string) (domain.Rate, error)
	}

	CurrencyRepository interface {
		GetAvailableCurrencies(ctx context.Context) ([]domain.Currency, error)
	}

	FastForexClient interface {
		GetFiatRates(fromCurrency string, toCurrencies []string) (model.Rates, error)
		GetCryptoRates(fromCurrency string, toCurrencies []string) (model.Rates, error)
	}
)
