package service

import (
	"context"

	"exchanger/internal/domain"
)

type (
	currencyService struct {
		currencyRepo CurrencyRepository
	}
)

func NewCurrencyService(currencyRepo CurrencyRepository) *currencyService {
	return &currencyService{
		currencyRepo: currencyRepo,
	}
}

func (s *currencyService) GetAvailableCurrencies(ctx context.Context) ([]domain.Currency, error) {
	return s.currencyRepo.GetAvailableCurrencies(ctx)
}
