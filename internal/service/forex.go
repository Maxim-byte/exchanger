package service

import (
	"context"
	"fmt"

	"exchanger/internal/service/model"
)

type (
	fastForexService struct {
		forexApiService    FastForexClient
		currencyRepository CurrencyRepository
	}
)

func NewFastForexService(service FastForexClient, repository CurrencyRepository) *fastForexService {
	return &fastForexService{
		forexApiService:    service,
		currencyRepository: repository,
	}
}

func (s *fastForexService) GetRates(ctx context.Context) ([]model.Rate, error) {
	currencies, err := s.currencyRepository.GetAvailableCurrencies(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to get available currencies: %w", err)
	}

	var fiatCurrencies, cryptoCurrencies []string
	for _, currency := range currencies {
		switch currency.CurrencyType {
		case model.FiatCurrencyType:
			fiatCurrencies = append(fiatCurrencies, currency.CurrencyCode)
		case model.CryptoCurrencyType:
			cryptoCurrencies = append(cryptoCurrencies, currency.CurrencyCode)
		}
	}

	result := make([]model.Rate, 0, len(fiatCurrencies)+len(cryptoCurrencies))

	for _, fiatCurrency := range fiatCurrencies {
		response, err := s.forexApiService.GetFiatRates(fiatCurrency, cryptoCurrencies)
		if err != nil {
			return nil, fmt.Errorf("failed to get rate for fiat currency: %w", err)
		}

		result = append(result, model.Rate{
			FromCurrency: fiatCurrency,
			ToCurrencies: response.Rates,
		})
	}

	for _, cryptoCurrency := range cryptoCurrencies {
		response, err := s.forexApiService.GetCryptoRates(cryptoCurrency, fiatCurrencies)
		if err != nil {
			return nil, fmt.Errorf("failed to get rate for fiat currency: %w", err)
		}

		result = append(result, model.Rate{
			FromCurrency: cryptoCurrency,
			ToCurrencies: response.Rates,
		})
	}

	return result, nil
}
