package domain

import (
	"time"

	"github.com/google/uuid"
)

type (
	Rate struct {
		ID           uuid.UUID
		Rate         float64
		UpdatedAt    time.Time
		ToCurrency   string
		FromCurrency string
	}
)
