package domain

import (
	"time"

	"github.com/google/uuid"
)

type (
	Currency struct {
		ID           uuid.UUID
		CreatedAt    time.Time
		UpdatedAt    time.Time
		IsAvailable  bool
		CurrencyName string
		CurrencyCode string
		CurrencyType string
	}
)
