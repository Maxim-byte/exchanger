package repository

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"exchanger/internal/domain"
	"exchanger/internal/service/model"
)

type (
	rateRepository struct {
		connection *gorm.DB
	}

	exchangeRate struct {
		ID           uuid.UUID `gorm:"column:id"`
		Rate         float64   `gorm:"column:rate"`
		UpdatedAt    time.Time `gorm:"column:updated_at"`
		ToCurrency   string    `gorm:"column:to_currency"`
		FromCurrency string    `gorm:"column:from_currency"`
	}
)

func NewRatesRepository(connection *gorm.DB) *rateRepository {
	return &rateRepository{
		connection: connection,
	}
}

func (r *rateRepository) GetRate(ctx context.Context, fromCurrency string, toCurrency string) (domain.Rate, error) {
	var exchangeRate exchangeRate
	err := r.connection.WithContext(ctx).
		First(&exchangeRate, "from_currency = ? AND to_currency = ?", fromCurrency, toCurrency).Error
	switch {
	case errors.Is(err, gorm.ErrRecordNotFound):
		return domain.Rate{}, model.ErrNotFound
	case err != nil:
		return domain.Rate{}, err
	}

	return domain.Rate{
		ID:           exchangeRate.ID,
		Rate:         exchangeRate.Rate,
		UpdatedAt:    exchangeRate.UpdatedAt,
		ToCurrency:   exchangeRate.ToCurrency,
		FromCurrency: exchangeRate.FromCurrency,
	}, nil
}

func (r *rateRepository) UpdateRates(ctx context.Context, rates []domain.Rate) error {
	exchangeRates := make([]exchangeRate, 0, len(rates))
	for _, rate := range rates {
		exchangeRates = append(exchangeRates, exchangeRate{
			ID:           uuid.New(),
			Rate:         rate.Rate,
			UpdatedAt:    rate.UpdatedAt,
			ToCurrency:   rate.ToCurrency,
			FromCurrency: rate.FromCurrency,
		})
	}

	return r.connection.WithContext(ctx).Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "from_currency"}, {Name: "to_currency"}},
		DoUpdates: clause.AssignmentColumns([]string{"rate", "updated_at"}),
	}).Create(&exchangeRates).Error
}
