package repository

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"

	"exchanger/internal/domain"
)

type (
	currencyRepository struct {
		connection *gorm.DB
	}

	Currency struct {
		ID           uuid.UUID `gorm:"column:id"`
		CurrencyName string    `gorm:"column:currency_name"`
		CurrencyCode string    `gorm:"column:currency_code"`
		CurrencyType string    `gorm:"column:currency_type"`
		IsAvailable  bool      `gorm:"column:is_available"`
		CreatedAt    time.Time `gorm:"column:created_at"`
		UpdatedAt    time.Time `gorm:"column:updated_at"`
	}
)

func NewCurrenciesRepository(connection *gorm.DB) *currencyRepository {
	return &currencyRepository{
		connection: connection,
	}
}

func (r *currencyRepository) GetAvailableCurrencies(ctx context.Context) ([]domain.Currency, error) {
	var currencies []Currency
	if err := r.connection.WithContext(ctx).Where("is_available = ?", true).Find(&currencies).Error; err != nil {
		return nil, err
	}

	domainCurrency := make([]domain.Currency, 0, len(currencies))
	for _, currency := range currencies {
		domainCurrency = append(domainCurrency, domain.Currency{
			ID:           currency.ID,
			CurrencyName: currency.CurrencyName,
			CurrencyCode: currency.CurrencyCode,
			CurrencyType: currency.CurrencyType,
			IsAvailable:  currency.IsAvailable,
			CreatedAt:    currency.CreatedAt,
			UpdatedAt:    currency.UpdatedAt,
		})
	}

	return domainCurrency, nil
}
