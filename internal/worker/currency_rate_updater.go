package worker

import (
	"context"
	"log/slog"
	"time"

	"exchanger/internal/domain"
)

type (
	currencyRateUpdater struct {
		ticker       *time.Ticker
		rateService  RateService
		forexService ForexService
	}
)

func NewCurrencyRateUpdater(rateService RateService, forexService ForexService, period time.Duration) *currencyRateUpdater {
	return &currencyRateUpdater{
		ticker:       time.NewTicker(period),
		rateService:  rateService,
		forexService: forexService,
	}
}

func (w *currencyRateUpdater) Run(ctx context.Context) {
	go func() {
		for range w.ticker.C {
			rates, err := w.forexService.GetRates(ctx)
			if err != nil {
				slog.Error("failed to get rates", slog.String("error", err.Error()))
				continue
			}

			if len(rates) == 0 {
				continue
			}

			domainRates := make([]domain.Rate, 0, len(rates))
			for _, r := range rates {
				for currency, rate := range r.ToCurrencies {
					domainRates = append(domainRates, domain.Rate{
						Rate:         rate,
						UpdatedAt:    time.Now(),
						ToCurrency:   currency,
						FromCurrency: r.FromCurrency,
					})
				}
			}

			if err := w.rateService.UpdateRates(ctx, domainRates); err != nil {
				slog.Error("failed to update rates", slog.String("error", err.Error()))
			}
		}
	}()
}

func (w *currencyRateUpdater) Shutdown() {
	w.ticker.Stop()
}
