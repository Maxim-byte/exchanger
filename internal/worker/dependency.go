package worker

import (
	"context"

	"exchanger/internal/domain"
	"exchanger/internal/service/model"
)

type (
	RateService interface {
		UpdateRates(ctx context.Context, rates []domain.Rate) error
	}

	ForexService interface {
		GetRates(ctx context.Context) ([]model.Rate, error)
	}
)
