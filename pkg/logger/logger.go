package logger

import (
	"log/slog"
	"os"
)

const (
	txtFormat  = "txt"
	jsonFormat = "json"
)

func GetLogHandler(logFormat string, logLevel string) slog.Handler {
	level := GetLogLevel(logLevel)

	handler := slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: level,
	})

	switch logFormat {
	case txtFormat:
		return handler
	case jsonFormat:
		return slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
			Level: level,
		})
	default:
		slog.Error("Unknown log format, Set to 'txt'", slog.String("log_format", logFormat))
	}

	return handler
}

func GetLogLevel(logLevel string) slog.Level {
	switch logLevel {
	case "debug":
		return slog.LevelDebug
	case "info":
		return slog.LevelInfo
	case "warn":
		return slog.LevelWarn
	case "error":
		return slog.LevelError
	default:
		slog.Default().Error("Unknown log level. Set to 'info'", slog.String("log_level", logLevel))
		return slog.LevelInfo
	}
}
