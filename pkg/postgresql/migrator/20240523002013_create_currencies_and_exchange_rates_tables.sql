-- +goose Up
-- +goose StatementBegin
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE currencies
(
    id            UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    currency_name VARCHAR(50) NOT NULL,
    currency_code VARCHAR(10) NOT NULL UNIQUE,
    currency_type VARCHAR(10) NOT NULL CHECK (currency_type IN ('crypto', 'fiat')),
    is_available  BOOLEAN     NOT NULL DEFAULT TRUE,
    created_at    TIMESTAMP            DEFAULT CURRENT_TIMESTAMP,
    updated_at    TIMESTAMP            DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX idx_currency_name ON currencies (currency_name);
CREATE INDEX idx_is_available ON currencies (is_available);
CREATE INDEX idx_currency_type ON currencies (currency_type);

CREATE TABLE exchange_rates
(
    id            UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    from_currency VARCHAR(10) NOT NULL,
    to_currency   VARCHAR(10) NOT NULL,
    rate          NUMERIC     NOT NULL,
    updated_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_from_currency FOREIGN KEY (from_currency) REFERENCES currencies (currency_code),
    CONSTRAINT fk_to_currency FOREIGN KEY (to_currency) REFERENCES currencies (currency_code),
    CONSTRAINT unique_from_to_currency UNIQUE (from_currency, to_currency)
);

CREATE INDEX idx_from_currency ON exchange_rates (from_currency);
CREATE INDEX idx_to_currency ON exchange_rates (to_currency);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP INDEX IF EXISTS idx_currency_name;
DROP INDEX IF EXISTS idx_is_available;
DROP INDEX IF EXISTS idx_currency_type;
DROP INDEX IF EXISTS idx_from_currency;
DROP INDEX IF EXISTS idx_to_currency;

DROP TABLE IF EXISTS exchange_rates;
DROP TABLE IF EXISTS currencies;
-- +goose StatementEnd