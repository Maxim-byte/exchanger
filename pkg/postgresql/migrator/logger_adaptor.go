package migrator

import (
	"log/slog"
	"os"
)

type GooseLoggerAdaptor struct {
	wrappedLogger *slog.Logger
}

func NewGooseLoggerAdaptor(logger *slog.Logger) *GooseLoggerAdaptor {
	return &GooseLoggerAdaptor{
		wrappedLogger: logger,
	}
}

func (l *GooseLoggerAdaptor) Fatalf(format string, v ...any) {
	l.wrappedLogger.Info(format, v...)
	l.wrappedLogger.Error("", v...)
	os.Exit(-1)
}

func (l *GooseLoggerAdaptor) Printf(format string, v ...any) {
	l.wrappedLogger.Info(format, v...)
}
