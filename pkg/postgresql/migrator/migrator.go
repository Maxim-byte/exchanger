package migrator

import (
	"database/sql"
	"fmt"
	"log/slog"

	"github.com/pressly/goose/v3"
)

const MigrationsTableName = "db_version"

type Migrator struct {
	connection *sql.DB
	logger     *slog.Logger
}

func NewMigrator(connection *sql.DB, logger *slog.Logger) *Migrator {
	return &Migrator{
		connection: connection,
		logger:     logger,
	}
}

func (m *Migrator) Up() error {
	goose.SetTableName(MigrationsTableName)
	goose.SetBaseFS(Migrations)
	goose.SetLogger(NewGooseLoggerAdaptor(m.logger))

	if err := goose.SetDialect("postgres"); err != nil {
		return fmt.Errorf("set dialect: %w", err)
	}

	if err := goose.Up(m.connection, "."); err != nil {
		return fmt.Errorf("apply migrations: %w", err)
	}

	return nil
}
